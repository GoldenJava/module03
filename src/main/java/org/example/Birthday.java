package org.example;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Birthday {
    public static long getAge(int year, int month, int date){
        LocalDate date1 = LocalDate.of(year, month, date);
        LocalDate date2 = LocalDate.now();
        return ChronoUnit.DAYS.between(date1, date2);
    }
    public static LocalDate nextBirthday(int year, int month, int date){
        LocalDate date1 = LocalDate.of(year, month, date);
        LocalDate date2 = LocalDate.now();
        long a = getAge(year, month, date) % 1000;
        return date2.plus(1000 - a ,ChronoUnit.DAYS);

    }
}
