package org.example;

public class CatFactory {

    public static Cat CreateCat(String name, int weight, boolean isAngry) throws IncorrectCatWeightException {
        try {
            Cat cat = new Cat(name, weight, isAngry);
            return cat;
        } catch (IncorrectCatWeightException e) {
            Cat cat = new Cat(name, 5, true);
            return cat;
        }
    }

}